package DroneSimulation;

import java.util.Random;

/**
 * Provides a class and enum for the direction a drone is moving towards
 *
 * @author Aisha Ali
 */
public class EnumGetRanDir{
    public static void main(String[] args){
        System.out.println(Direction.getRandomDirection());
    }
}

enum Direction {
    NORTH,
    EAST,
    SOUTH,
    WEST;

    /**
     * finds the enum value.
     */
    public static Direction getRandomDirection(){
        Random random = new Random();
        return values()[random.nextInt(values().length)];
    }

    /**
     * Produces a random number from within a range that excludes specific integers.
     */
    public static int getRandomInt(int start, int end, int... exclude){
        Random random = new Random();
        int randomInt = start + random.nextInt(end - start + 1 - exclude.length);
        for (int ex : exclude) {
            if (randomInt < ex) {
                break;
            }
            randomInt++;
        }
        return randomInt;
    }
}

