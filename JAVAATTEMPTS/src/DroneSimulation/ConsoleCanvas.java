package DroneSimulation;

import java.util.Arrays;

/**
 * This class has the objects that represent a DroneArena object of the right
 * size with a boundary wall and indicators marking the pos of individual drones within the arena
 * that is then displayed within the console.
 * @author Aisha Ali

 */
public class ConsoleCanvas {
    char[][] canvas;
    DroneArena d_arena;

    /**
     * Constructs ConsoleCanvas objects to represent provided DroneArena.
     * x  y length nd height of cArena
     * cArena DroneArena object to represent
     */
    public ConsoleCanvas(int x, int y, DroneArena cArena){
        this.d_arena = cArena;
        this.canvas = new char[y+2][x+2];
        Arrays.fill(this.canvas[0], '#');
        Arrays.fill(this.canvas[this.canvas.length-1], '#');

        for(int i = 1; i < this.canvas.length-1; i++){
            for(int h = 0; h < this.canvas[i].length; h++){
                if(h == 0 || h == this.canvas[i].length-1){
                    this.canvas[i][h] = '#';
                }
                else{
                    this.canvas[i][h] = ' ';
                }
            }
        }
    }

    /**
     * Inserts a drone into the canvas at the requested coordinates and portrays an indicator at
     * every location a drone is present.
     *  x  y position along length nd height of droneArena object
     * d Direction enum value showing direction of drone travel
     * c Character to use as indicator of drone presence at a location
     */
    public void showIt(int x, int y, Direction d, char c){
        d_arena.addDrone(x, y, d);
        for(int i = 1; i < this.canvas.length-1; i++){
            for(int h = 1; h < this.canvas[i].length-1; h++){
                if(d_arena.getDroneAt(h-1,i-1)){
                    this.canvas[i][h] = c;
                }
            }
        }
    }

    /**
     * Method for drawing the canvas.
     */
    public String toString(){
        StringBuilder canvasDisplay = new StringBuilder();
        for (int i = 0; i < this.canvas.length; i++){
            canvasDisplay.append(this.canvas[i]);
            canvasDisplay.append('\n');
        }
        return this.d_arena.toString() + canvasDisplay.toString() + "\n";
    }

    public static void main(String[] args){
        DroneArena d = new DroneArena(10, 5);
        d.addDrone();
        ConsoleCanvas c = new ConsoleCanvas(15, 9, d);
        c.d_arena.showDrones(c);
        System.out.println(c.toString());
    }
}
