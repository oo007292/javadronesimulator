package DroneSimulation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

/**
 * The Drone Arena class implements arena objects that stores drone objects
 * has controls for the size of the arena, the number of drones and where
 * they are placed within the arena.
 *
 * @author Aisha Ali
 *
 */
public class DroneArena implements Serializable{
    private int length, height;
    ArrayList<Drone> drones;
    private Random gen;

    /**
     * DroneArena class constructor
     * Creates objects with set size parameters
     * x length of arena object
     * y height of arena object
     */
    public DroneArena(int x, int y){
        this.setSize(x, y);
        this.drones = new ArrayList<Drone>();
        this.gen = new Random();
    }

    public String getSize(){
        return "Length: " + this.getLength() + ", Height: " + this.getHeight();
    }

    public int getLength(){
        return this.length;
    }

    public int getHeight(){
        return this.height;
    }

    /**
     * Method for setting size of arena object
     * x length y height of arena object
     */
    public void setSize(int x, int y){
        setLength(x);
        setHeight(y);
    }

    private void setLength(int x){
        this.length = x;
    }

    private void setHeight(int y){
        this.height = y;
    }

    /**
     * a method for adding drone object to random location within arena
     */
    public void addDrone(){
        int x, y;
        do{
            x = gen.nextInt(this.length);
            y = gen.nextInt(this.height);
        }
        while(this.getDroneAt(x, y));
        this.drones.add(new Drone(x, y, Direction.getRandomDirection()));
    }

    /**
     * Overloaded method for adding drone to specified location within arena
     *  x position on x axis/length
     * y position on y axis/height
     */
    public void addDrone(int x, int y, Direction d){
        if(this.getDroneAt(x, y)){
            System.out.println("Drone is already present in this area. ");
            return;
        }
        this.drones.add(new Drone(x, y, d));
    }

    /**
     * Method for getting the size of arena and info on each drone
     */
    public String toString(){
        StringBuilder droneLocs = new StringBuilder();
        for (Drone drone : drones){
            droneLocs.append(drone.toString());
        }
        return "The Arena size is " + this.getSize() + "\n" + droneLocs;
    }

    /**
     * Checks if a drone is at specified location within arena
     * x pos on x axis/length
     * y pos on y axis/height
     * true if location at x y has something in it already, false if empty
     */
    public boolean getDroneAt(int x, int y){
        for (Drone drone : drones) {
            if (drone.isHere(x, y)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if a drone can move to a specified location within arena
     * x pos on x axis/length
     * y pos on y axis/height
     * returns true if the location is within arena boundaries and will not contain another drone
     */
    public boolean canMoveHere(int x, int y){
        return (x <= this.length-1 && x >= 0) && (y <= this.height-1 && y >= 0) && !this.getDroneAt(x, y);
    }

    /**
     * Moves all drones within the arena
     * arena that contains the drones, allows the arena to pass itself
     * when the method is called from another class object
     */
    public void moveAllDrones(DroneArena arena){
        for(Drone drone : arena.drones){
            drone.tryToMove(arena);
        }
    }

    public void animatingDrones() {
        for (int i = 0; i < 10; i++) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            //moveAllDrones();                                    // Drones moved
            //showDrones();

        }
    }

    /**
     * Displays indicators for all drones contained in the arena
     */
    public void showDrones(ConsoleCanvas c){
        for (Drone drone : drones){
            drone.displayDrone(c);
        }
    }

    public static void main(String[] args){
        DroneArena one = new DroneArena(20,10);
        one.addDrone();
        one.addDrone(10, 10, Direction.WEST);
        System.out.println(one.getDroneAt(10, 10));
        System.out.println(one.toString());
    }
}

