package DroneSimulation;

import java.io.Serializable; // writing stuff into files
import java.util.UUID;

/**
 * The Drone class implements objects that represent drones
 * and controls how they move and retrieves information
 * Gets info on their positions nd their unique identifiers .
 *
 * @author Aisha
 */
public class Drone implements Serializable {
    private int x, y;
    private Direction direction;
    private static int count = 0;
    private int id;

    /**
     * Constructs Drone objects at the specified coordinates
     * x pos on the x axis
     * y pos on the y axis
     */
    public Drone(int x, int y, Direction d){
        //Setting current position and direction of travel allows for tracking and drawing the drone's movement
        this.x = x;
        this.y = y;
        this.direction = d;
        count += 1;
        this.id = count; // as a new Drone is added a new id will be created

    }

    /**
     * gets pos of Drone object
     *
     */
    public String getPosition(){
        return this.getX() + ", " + this.getY();
    }

    public int getX(){
        return this.x;
    }

    public int getY(){
        return this.y;
    }

    /**
     * gets unique identifier of Drone object
     *
     */
    public int getId(){
        return this.id;
    }

    /**
     * Method for setting x and y coordinates of Drone object
     * x pos on the x axis
     * y pos on the y axis
     */
    public void setPosition(int x, int y){
        this.x = x;
        this.y = y;
    }

    /**
     * gets info on Drone unique identifier and coordinates
     *
     */
    public String toString(){
        return "Drone " + this.getId() + " is at X, Y " + this.getPosition() + " and moving " + this.direction + "\n";
    }


    public boolean isHere(int sx, int sy){ // checks if drone is present at coordinates and will return tru otherwise false
        return this.x == sx && this.y == sy;
    }

    /**
     * Moves drones around an arena in a specified distance
     * move according to their direction given
     * and will turn if the distance would move them out of the arena bounds or collide with another drone.
     * arena DroneArena object that the drones are contained in.
     */
    public void tryToMove(DroneArena arena){
        int changeX = this.x;
        int changeY = this.y;
        int dirIndex = 0;

        //Change coordinates used for arena and canvas positioning
        switch (this.direction) {
            case NORTH -> changeY -= 1;
            case EAST -> {
                changeX += 1;
                dirIndex = 1;
            }
            case SOUTH -> {
                changeY += 1;
                dirIndex = 2;
            }
            case WEST -> {
                changeX -= 1;
                dirIndex = 3;
            }
        }
        //Check if projected space available
        if (arena.canMoveHere(changeX, changeY)) {
            this.x = changeX;
            this.y = changeY;
        }
        //When blocked turn in random direction
        else {
            this.direction = Direction.values()[Direction.getRandomInt(0, 3, dirIndex)];
            System.out.println("Drone " + this.id + " Turning " + this.direction + "\n");
        }
    }

    /**
     * Places indicator of the drone within the console canvas to match it's current position.
     * @param c ConsoleCanvas object that will present the drone's position.
     */
    public void displayDrone(ConsoleCanvas c){
        for(int i = 1; i < c.canvas.length-1; i++){
            for(int j = 1; j < c.canvas[i].length-1; j++){
                if(c.d_arena.getDroneAt(j-1,i-1)){
                    c.canvas[i][j] = 'D';
                }
            }
        }
    }

    public static void main(String[] args){
        Drone one = new Drone(6,9, Direction.getRandomDirection());
        Drone two = new Drone(4,2, Direction.getRandomDirection());
        System.out.println(one.toString());
        System.out.println(two.toString());


    }
}
