package DroneSimulation;

import java.io.*;
import javax.swing.*;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import javax.swing.filechooser.FileNameExtensionFilter;



/**
 * This class gives a text based interface within the console to allow management of the arena and it's
 * associated drones and console canvas.
 *
 * @author Aisha Ali
 */

public class DroneInterface{
    private DroneArena myArena;
    ConsoleCanvas myCanvas;

    /**
     * Handles keyboard input and maps keys to the different functions in the program .
     */
    public DroneInterface(){
        final Scanner s;

        File dir = new File(".\\saves");
        if(!dir.exists()){
            dir.mkdir();
        }
        s = new Scanner(System.in);
        myArena = new DroneArena(20, 6);
        char ch;
        do {
            System.out.print("Enter (A)dd drone, (P)lay animation, get (I)nformation, (D)isplay drones, (M)ove drones, " +
                    "(N)ew arena, (S)ave arena, (L)oad arena or e(X)it :) > ");
            ch = s.next().charAt(0);
            s.nextLine();
            switch (ch) {
                case 'd', 'D' -> this.doDisplay();
                case 'A', 'a' -> myArena.addDrone();
                case 'P', 'p' -> myArena.animatingDrones();
                case 'I', 'i' -> System.out.print(myArena.toString());
                case 'M', 'm' -> {
                    System.out.println("Enter an integer for the distance to move:");
                    int distance = 0;
                    try {
                        distance = s.nextInt();
                    }
                    catch(Exception e) {
                        System.err.println("Error. Invalid input, please enter an integer.");
                    }

                    if(!myArena.drones.isEmpty() && distance != 0){
                        int i = 1;
                        while(i <= distance){
                            myArena.moveAllDrones(myArena);
                            doDisplay();
                            try{
                                TimeUnit.MILLISECONDS.sleep(250);
                            }
                            catch (InterruptedException exception){
                                System.err.format("Exception: %s%n", exception);
                            }
                            i++;
                        }
                        System.out.println("\n");
                    }
                    else {
                        System.err.println("The Arena is empty!");
                    }
                }
                case 'N', 'n' -> {
                    try {
                        System.out.println("Entering the new arena dimensions:\n Please enter the X integer value.\n");
                        int x = s.nextInt();
                        System.out.println("Please enter the Y integer value.");
                        int y = s.nextInt();
                        this.myArena = new DroneArena(x, y);
                    } catch (Exception e) {
                        System.err.println("Error. Invalid input, please enter an integer.");
                    }
                }
                case 'S', 's' -> {
                    int response;
                    File saveFile;

                    JFileChooser fileChooser = new JFileChooser(".\\saves");
                    FileNameExtensionFilter filter = new FileNameExtensionFilter(".ser", "ser");
                    fileChooser.setFileFilter(filter);
                    fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

                    response = fileChooser.showSaveDialog(null);
                    FileOutputStream fOutStream;
                    ObjectOutputStream oOutStream;
                    if(response == JFileChooser.APPROVE_OPTION){
                        saveFile = fileChooser.getSelectedFile();
                        try {
                            fOutStream = new FileOutputStream(saveFile);
                            oOutStream = new ObjectOutputStream(fOutStream);
                            oOutStream.writeObject(this.myArena);
                        }
                        catch (IOException e) {
                            System.out.println("An error occurred.");
                            e.printStackTrace();
                        }

                    }
                }
                case 'L', 'l' -> {
                    int response;
                    File loadFile;

                    JFileChooser fileChooser = new JFileChooser(".\\saves");
                    fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
                    FileNameExtensionFilter filter = new FileNameExtensionFilter(".ser", "ser");
                    fileChooser.setFileFilter(filter);

                    response = fileChooser.showOpenDialog(null);
                    if(response == JFileChooser.APPROVE_OPTION){
                        loadFile = fileChooser.getSelectedFile();
                        if(loadFile.isFile()){
                            try {
                                FileInputStream fInStream = new FileInputStream(loadFile);
                                ObjectInputStream oInStream = new ObjectInputStream(fInStream);
                                this.myArena = (DroneArena) oInStream.readObject();
                            }
                            catch (IOException | ClassNotFoundException e) {
                                System.out.println("An error occurred.");
                                e.printStackTrace();
                            }
                        }
                    }
                }
                case 'x' -> ch = 'X';
            }
        } while (ch != 'X');

        s.close();
    }

    /**
     * Draws  the ConsoleCanvas to the size appropriate for the arena and marks drone locations before portraying
     * the canvas to the console.
     */
    void doDisplay(){
        int x = myArena.getLength();
        int y = myArena.getHeight();
        this.myCanvas = new ConsoleCanvas(x, y, this.myArena);
        this.myArena.showDrones(this.myCanvas);

        System.out.print(this.myCanvas.toString());
    }

    public static void main(String[] args){
        DroneInterface r = new DroneInterface();
    }
}

